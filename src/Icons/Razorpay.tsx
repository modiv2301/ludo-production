import { IonAvatar, IonItem, IonLabel } from '@ionic/react';
import React, { useCallback } from 'react';
import useRazorpay from "react-razorpay";
import razorpay from './../assets/img/Razorpay-logo.png'

interface RpayProps {
    style?: React.CSSProperties;
};


export const Razorpay = (props: RpayProps) => {

    const Razorpay = useRazorpay();

    const  displayRazorpay = useCallback(() => {

        const options = {
            key: "rzp_test_GYl71wgx7cqEWs",
            amount: "50000",
            currency: "INR",
            name: "Ludo Project",
            description: "Your Transaction",
            image: razorpay,
            order_id: "",
            handler:(response:any)=>{
                console.log("my response is =>" + response)
                // alert(response.razorpay_payment_id);
                // alert(response.razorpay_order_id);
                // alert(response.razorpay_signature);
            },
            prefill: {
                name: 'vivek modi',
                email: 'example@example.com',
                contact: '9999999999',
            },
            notes: {
                address: 'Example Corporate Office',
            },
            theme: {
                color: '#61dafb',
            },
        };

        const paymentObject = new Razorpay(options);

        // paymentObject.on("payment.failed", function (response: any) {
        //     alert(response.error.code);
        //     alert(response.error.description);
        //     alert(response.error.source);
        //     alert(response.error.step);
        //     alert(response.error.reason);
        //     alert(response.error.metadata.order_id);
        //     alert(response.error.metadata.payment_id);
        // });

        paymentObject.open();

    },[Razorpay]);


    return (
        <IonItem style={{ border: '4px solid lightgray', borderRadius: "15px" }}>
            <IonAvatar style={props.style}>
                <img src={razorpay} />
            </IonAvatar>
            <IonLabel style={{ fontWeight: 'bold', paddingLeft: '10px', color: 'gray' }} onClick={displayRazorpay}>Razorpay</IonLabel>
        </IonItem>
    )
};



        // const res = await loadScript(
        //     'https://checkout.razorpay.com/v1/checkout.js'
        // );

        // if (!res) {
        //     alert('Razorpay SDK failed to load. Are you online?');
        //     return;
        // }

        // const result = await axios.post('/payment/orders');

        // if (!result) {
        //     alert('Server error. Are you online?');
        //     return;
        // }

        // const { amount, id: order_id, currency } = result.data;


        
                // const data = {
                //   orderCreationId: order_id,
                //   razorpayPaymentId: response.razorpay_payment_id,
                //   razorpayOrderId: response.razorpay_order_id,
                //   razorpaySignature: response.razorpay_signature,
                // };

                // const result = await axios.post('/payment/success', data);

                // alert(result.data.msg);

                // alert(response.razorpay_payment_id);
                // alert(response.razorpay_order_id);
                // alert(response.razorpay_signature);

                // function loadScript(src: any) {
                //     return new Promise((resolve) => {
                //         const script = document.createElement('script');
                //         script.src = src;
                //         script.onload = () => {
                //             resolve(true);
                //         };
                //         script.onerror = () => {
                //             resolve(false);
                //         };
                //         document.body.appendChild(script);
                //     });
                // }

                
    // async function displayRazorpayPaymentSdk() {
    //     const res = await loadRazorpayScript(
    //         "https://checkout.razorpay.com/v1/checkout.js"
    //     );

    //     if (!res) {
    //         alert("Razorpay SDK failed to load. please check are you online?");
    //         return;
    //     }

    //     // creating a new order and sending order ID to backend
    //     const result = await axios.post("http://127.0.0.1:8000/razorpay_order", {
    //         "order_id" : "Order-5152"
    //     });

    //     if (!result) {
    //         alert("Server error. please check are you onlin?");
    //         return;
    //     }

    //     // Getting the order details back
    //      const {merchantId=null , amount=null,currency=null,orderId=null } = result.data;

    //     const options = {
    //         key: merchantId,
    //         amount: amount.toString(),
    //         currency: currency,
    //         name: "Razorpay Testing",
    //         description: "Test Transaction",
    //         order_id: orderId,
    //         callback_url: "http://127.0.0.1:8000/razorpay_callback",
    //         redirect: true,
    //         prefill: {
    //           name: "Swapnil Pawar",
    //           email: "swapnil@example.com",
    //           contact: "9999999999",
    //       },
    //         notes: {
    //             address: "None",
    //         },
    //         theme: {
    //             color: "#61dafb",
    //         },
    //     };

    //     const paymentObject = new Razorpay(options);
    //     paymentObject.open();
    //   }

    // function loadRazorpayScript(src:any) {
    //     return new Promise((resolve) => {
    //         const script = document.createElement("script");
    //         script.src = src;
    //         script.onload = () => {
    //             resolve(true);
    //         };
    //         script.onerror = () => {
    //             resolve(false);
    //         };
    //         document.body.appendChild(script);
    //     });
    // }