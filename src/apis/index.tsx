import axios, { AxiosRequestConfig, AxiosRequestHeaders } from "axios";
import { storeKeys } from "./constants";
const baseURL = "http://rassgroups.com/ludo/public/api/v1/";
// const baseURL = 'http://127.0.0.1:8000/api/v1';

const ngrokURL = "";

const instance = axios.create({ baseURL });

let user_id = localStorage.getItem("userid");

let storetoken = localStorage.getItem("usertoken");

instance.interceptors.request.use(function (config: AxiosRequestConfig) {
  const token = localStorage.getItem("usertoken") || "";
  if (token) {
    const headers: AxiosRequestHeaders = {
      // Authorization: token
      Authorization: `Bearer ${token}`,
    };
    config.headers = headers;
  }
  return config;
});

export interface AuthParams {
  mobile_no: string;
  otp: string;
  device_id: string;
}

export interface LoginParam {
  mobile_no: string;
}

export interface SignupParams {
  name: string;
  email: string;
  password: string;
}

export const auth = (form: AuthParams) =>
  instance({
    method: "POST",
    url: `/customer/login-with-otp`,
    data: form,
    responseType: "json",
  });

export const sendOtpForLogin = (form: LoginParam) =>
  instance({
    method: "POST",
    url: `/customer/login-send-otp`,
    data: form,
    responseType: "json",
  });

export const registerSendOtp = (form: SignupParams) =>
  instance({
    method: "POST",
    url: `/customer/register-send-otp`,
    data: form,
    responseType: "json",
  });

export const register = (form: SignupParams) =>
  instance({
    method: "POST",
    url: `/customer/register-with-otp`,
    data: form,
    responseType: "json",
  });

export const allgame = (params = "") =>
  instance({
    method: "GET",
    url: `/customer/allgame?type=` + params,
    responseType: "json",
  });

export const postwallet = () =>
  instance({
    method: "POST",
    url: `customer/wallet`,
    responseType: "json",
  });

export const wallet = (user_id: any) =>
  instance({
    method: "GET",
    data: user_id,
    url: `/customer/wallet_history`,
    responseType: "json",
  });

export const postscreenshot = (game_id: any, data: any) =>
  instance({
    method: "POST",
    data: data,
    url: `customer/games/histories/${game_id}/approval`,
    responseType: "json",
  });

export const addCash = (data: any) =>
  instance({
    method: "POST",
    url: `/customer/wallet`,
    data: data,
    responseType: "json",
  });

export const profileChange = (data: any) =>
  instance({
    method: "POST",
    url: `/customer/update-profile-image`,
    data: data,
    responseType: "json",
  });

export const getRoomCode = (battleId: any) =>
  instance({
    method: "GET",
    url: `/customer/allbattle/${battleId}`,
    responseType: "json",
  });

export const getNotification = () =>
  instance({
    method: "GET",
    url: `/customer/notifications`,
    responseType: "json",
  });

export const getGameHistory = () =>
  instance({
    method: "GET",
    url: `/customer/games/histories`,
    responseType: "json",
  });

export const getProfile = () =>
  instance({
    method: "GET",
    url: `/customer/profile`,
    responseType: "json",
  });

export const getprofilewithdrawamount = (user_id: any) =>
  instance({
    method: "GET",
    data: user_id,
    url: `/customer/profile`,
    responseType: "json",
  });

export const postwithdrawamount = (data: any) =>
  instance({
    method: "POST",
    data: data,
    url: `/customer/wallets/withdraw`,
    responseType: "json",
  });

export const getentryamount = () =>
  instance({
    method: "GET",
    url: `/customer/allgame`,
    responseType: "json",
  });
