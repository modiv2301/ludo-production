import classes from './index.module.scss';
import { IonAvatar } from '@ionic/react';
import { Box, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { CgProfile } from 'react-icons/cg';
import money from './../../assets/img/money.svg';
import male from './../../assets/img/male.png';
import female from './../../assets/img/female.png';
import battle from './../../assets/img/energy.png';
import { useHistory } from 'react-router';

import {
    IonCardContent,
    IonCard,
    IonButton,
    IonIcon,
} from "@ionic/react";

import { FaRupeeSign } from "react-icons/fa";
import { allgame, getprofilewithdrawamount, postwithdrawamount, getentryamount } from '../../apis';


interface BattleCardProps {
    username1?: string;
    username2?: string;
    amount?: number;
    onClick?: Function;
};

export const BattleCard = (props: BattleCardProps) => {

    let [battleList, setBattleList] = useState<any[]>([]);

    let [userprofile, setUserprofile] = useState<any[]>([]);

    const history = useHistory();

    const getallgame = async () => {
        try {
            let response = await allgame();
            // console.log("my battle record" + JSON.stringify(response.data.data));
            setBattleList(response.data.data)

        } catch (error) {
            console.error("Getting error at auth: ", error);

        }
    };


    const getuserprofile = async () => {
        try {
            let data = {
                "user_id": localStorage.getItem("userid"),
            }
            let response = await getprofilewithdrawamount(data);
            // console.log("get user profile wallet_amount =>" + JSON.stringify(response.data.data.data.wallet_amount));
            setUserprofile(response.data.data.data.wallet_amount)
        } catch (error) {
            console.error("Getting error at auth: ", error);
        }
    }


   
    // const minusentryamount = async (id) =>{
    //     try {
    //         let response = await getentryamount();
    //         // console.log("get minus entry amount =>" + JSON.stringify(response.data.data.entry_amount));
    //         {
    //             response.data.data.filter(amount => amount.id == id).map((amount)=>(
    //                         console.log("my amount =>" + amount.entry_amount)
    //             ))
    //         }
    //         setentryamount(response.data.data)
    //     } catch (error) {
    //         console.error("Getting error at auth: ", error);
    //     }
    // }


    useEffect(() => {
        getallgame()
        getuserprofile()
    }, [])

    return (
        <Box onClick={(_e: any) => props.onClick ? props.onClick(_e) : undefined} sx={{
            borderRadius: '15px',
            margin: '.5rem',
            backgroundColor: '#f7f7f7',
        }}>


            <div
                style={{
                    marginTop: "5%",
                }}
            ></div>

            {
                battleList.map((title, index) => (
                    <IonCard key={index}>
                        <IonCardContent
                            style={{
                                textTransform: "uppercase",
                            }}
                        >
                            {title.game_name}
                            <span
                                style={{
                                    textTransform: "uppercase",
                                    float: "right",
                                }}
                            >
                                entry
                            </span>
                        </IonCardContent>

                        <IonCardContent
                            style={{
                                color: "5px solid black",
                            }}
                        >
                            <IonIcon
                                style={{ width: "2rem", height: "2rem", verticalAlign: "middle" }}
                                src={money}
                            />
                            &nbsp; {title.win_amount}
                            <span
                                style={{
                                    textTransform: "uppercase",
                                    float: "right",
                                    // marginTop: "-3%",
                                }}
                            >
                                <IonButton color="primary" id="btn"
                                    onClick={() =>
                                        history.push('/create-battle/' + title.id)

                                    }
                                >
                                    <FaRupeeSign />
                                    {title.entry_amount}
                                </IonButton>
                            </span>
                        </IonCardContent>
                    </IonCard>
                ))
            }
        </Box>
    )
}


// axios.post('http://localhost:8000/api/v1/customer/allgame')
// .then((response)=> {
//     console.log(response.data.data);
//     setBattleList(response.data.data);
// }) 
// .catch((error)=>{
//     console.log(error);
// })


{/* <IonFooter
    
                        id="footer"
    
                    >
                        <div
                            style={{
                                color: "red",
                                textTransform: "uppercase",
                            }}
    
                            id="play"
    
                        >
                            Playing Now
                        </div>
                    </IonFooter> */}

{/* <Typography variant="subtitle1" sx={{ fontWeight: 'bold', color: 'gray', paddingLeft: '1rem' }}>
                Playing for <IonIcon style={{ width: "3rem", height: "3rem", verticalAlign: "middle" }} src={money} /> <span style={{ fontSize: 'large', color: 'darkred' }}>{props.amount}</span>
            </Typography>
            <Box className={classes.battle} sx={{
                display: 'flex',
                justifyContent: 'center'
            }}>
                <div style={{ width: '20%' }}>
                    <div className={`${classes.avatar} ${classes.border}`}>
                        <img src={male} />
                    </div>
                    <span className={classes.bat}>{props.username1}</span>
                </div>
                <div style={{ width: '20%' }}>
                    <div className={classes.avatar}>
                        <img src={battle} />
                    </div>
                </div>
                <div style={{ width: '20%' }}>
                    <div className={`${classes.avatar} ${classes.border}`}>
                        <img src={female} />
                    </div>
                    <span className={classes.bat}>{props.username2}</span>
                </div>
            </Box> */}

{/* <IonCard>
                <IonCardContent
                    style={{
                        textTransform: "uppercase",
                    }}
                >
                    prize pool
                    <span
                        style={{
                            textTransform: "uppercase",
                            float: "right",
                        }}
                    >
                        entry
                    </span>
                </IonCardContent>

                <IonCardContent
                    style={{
                        color: "5px solid black",
                    }}
                >
                    <IonIcon
                        style={{ width: "2rem", height: "2rem", verticalAlign: "middle" }}
                        src={money}
                    />
                    &nbsp; 20
                    <span
                        style={{
                            textTransform: "uppercase",
                            float: "right",
                        }}
                    >
                        <IonButton color="primary" id="btn">
                            <FaRupeeSign />12
                        </IonButton>
                    </span>
                </IonCardContent>

                <IonFooter

                    id="footer"
                >
                    <div
                        style={{
                            color: "red",
                            textTransform: "uppercase",
                        }}
                        id="play"
                    >
                        Playing Now
                    </div>

                    <div
                        id="waiting"
                    >
                        Waiting now
                    </div>
                </IonFooter>
            </IonCard> */}
