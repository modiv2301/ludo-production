import { Container, InputAdornment, Stack, TextField, Typography, IconButton } from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { useState } from "react";

 const TextInput  = (props) => {
  const {formik, label, field_name, type = "text", className=""} = props
    let field_value =  formik.values && formik.values[field_name];
    let field_error =  formik.errors && formik.errors[field_name];
    const [showPassword, setShowPassword] = useState(false);
  
    const toggleShow = ()  => {
      return setShowPassword(!showPassword)
    }
        return (
      <div>
        <TextField
          label={label}
          id="outlined-size-small"
          name={field_name}
          size="medium"
          fullWidth
          onChange={formik.handleChange}
          value={field_value}
          color="primary"
                  error={field_error}
                  helperText={field_error}
          type={type === "password" ? showPassword ? "text" : "password" : type}
          className={className ? className : ""}
  
        />
      </div>
    );
  };

  export default TextInput