import "./index.scss";
import React, { useEffect, useState } from "react";
import { IonIcon, IonLabel, IonMenuButton, IonTitle } from "@ionic/react";
import { menu as menuIcon } from "ionicons/icons";

import { getprofilewithdrawamount } from "../../apis";
import { getProfile } from "../../apis";

interface HeaderProps {
  title?: string;
  icon?: any;
  rightAlignIcon?: React.ReactNode;
  rightAlignIconOnClick?: () => any;
  onIconClick?: () => any;
  children?: React.ReactNode;
  isMenu?: boolean;
}

export const Header = (props: HeaderProps) => {
  let [profileData, setProfileData] = useState({
    id: "",
    name: "",
    email: "",
    mobile_no: "",
    image: "",
    wallet_amount: "",
    last_login: "",
  });
  useEffect(() => {
    if (localStorage.getItem("usertoken")) {
      getProfile()
        .then((response) => {
          setProfileData(response.data.data.data);
        })
        .catch((error) => {
          console.error("Getting error at auth: ", error);
        });
    }
  }, []);

  return (
    <>
      <IonTitle className={"headerTitle"}>
        <IonLabel style={{ display: "flex", justifyContent: "space-between" }}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            {props.isMenu ? (
              <IonMenuButton
                slot="start"
                title="Login"
                style={{ float: "left" }}
              >
                <IonIcon className="menuIcon" icon={menuIcon} />
              </IonMenuButton>
            ) : undefined}
            {props.icon && (
              <IonIcon
                onClick={props.onIconClick}
                icon={props.icon}
                className="color-primary"
                style={{ fontSize: "xx-large", verticalAlign: "middle" }}
              />
            )}
            <span style={{ textAlign: "center", fontWeight: "bold" }}>
              {props.title}
            </span>
          </div>
          <div
            onClick={props.rightAlignIconOnClick}
            style={{
              marginTop: "3%",
            }}
          >
            {props.rightAlignIcon}
          </div>
          <div
            style={{
              marginTop: "5%",
            }}
          >
            {profileData.wallet_amount}
          </div>
        </IonLabel>
        {props.children}
      </IonTitle>
    </>
  );
};
