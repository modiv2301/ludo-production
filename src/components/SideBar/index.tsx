import "./index.scss";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import {
  IonMenu,
  IonHeader,
  IonToolbar,
  IonContent,
  IonList,
  IonItem,
  IonRouterOutlet,
  IonMenuToggle,
} from "@ionic/react";
import { IonAvatar, IonFooter, IonLabel, IonButton } from "@ionic/react";
import profile from "../../assets/img/profile.png";
import { AccountBalanceWalletSharp, HomeSharp } from "@mui/icons-material";
import { GiTrophyCup } from "react-icons/gi";
import { CgLogOut } from "react-icons/cg";
import { AiOutlineTransaction } from "react-icons/ai";
import {
  MdNotifications,
  MdOutlineContactSupport,
  MdAccountBalanceWallet,
} from "react-icons/md";
import { clearStore } from "../../apis/constants";
import { MdModeEditOutline } from "react-icons/md";
import { profileChange } from "../../apis";
import { Link } from "react-router-dom";

export const SideMenu = () => {
  const history = useHistory();

  const [userprofile, setprofile] = useState(profile);

  const handlechange = async (e: any) => {
    let formData = new FormData();
    formData.append("image", e.target.files[0]);
    // let image= e.target.files[0];
    // setprofile(URL.createObjectURL(e.target.files[0]));
    let walletAdd = await profileChange(formData);
  };

  const logout = () => {
    clearStore();
    localStorage.removeItem("usertoken");
    localStorage.removeItem("userimage");
    localStorage.removeItem("username");
    history.push("/login");
  };

  const onClick = async (path: string) => {
    history.push(path);
  };

  const handleInvite = () => {
    let url = `dkjsdjksdjk/signup/${JSON.parse("referral_code")}`;
  };
  return (
    <>
      <IonMenu swipeGesture={false} contentId="main">
        {/* <IonHeader>
          <IonToolbar>
            <div className="menu-header">
              <IonAvatar>
                <img src={userprofile} alt="" />
                <input
                  type="file"
                  id="imageUpload"
                  onChange={(e) => {
                    handlechange(e);
                  }}
                  style={{
                    display: "none",
                  }}
                />
                <span
                  style={{
                    position: "absolute",
                    top: 80,
                    left: 140,
                  }}
                >
                  <label
                    htmlFor="imageUpload"
                    style={{
                      padding: "10%",
                      borderRadius: "100%",
                      //  background: "#FFFFFF",
                      background: "white",
                      border: "1px solid transparent",
                      boxShadow: "0px 2px 4px 0px rgba(0, 0, 0, 0.12)",
                      cursor: "pointer",
                      fontWeight: "normal",
                      transition: "all 0.2s ease-in-out",
                    }}
                  >
                    <span style={{
                      position: 'relative',

                    }}>
                      <MdModeEditOutline
                        style={{
                          color: "black",
                          paddingTop: '18%',
                          fontSize: '18px'
                        }}
                      />
                    </span>
                  </label>
                </span>
              </IonAvatar>
              <IonAvatar></IonAvatar>
            </div>
          </IonToolbar>
        </IonHeader> */}
        <IonContent fullscreen>
          <div className={"profileList"}>
            <IonList>
              <IonMenuToggle onClick={() => onClick("/dashboard")}>
                <IonItem>
                  <IonLabel>
                    <HomeSharp /> Home
                  </IonLabel>
                </IonItem>
              </IonMenuToggle>
              <IonMenuToggle onClick={() => onClick("/wallet")}>
                <IonItem>
                  <IonLabel>
                    <AccountBalanceWalletSharp /> My Wallet
                  </IonLabel>
                </IonItem>
              </IonMenuToggle>
              <IonMenuToggle onClick={() => onClick("/game-history")}>
                <IonItem>
                  <IonLabel>
                    <GiTrophyCup /> Game History
                  </IonLabel>
                </IonItem>
              </IonMenuToggle>
              <IonMenuToggle onClick={() => onClick("/notifications")}>
                <IonItem>
                  <IonLabel>
                    <MdNotifications /> Notifications
                  </IonLabel>
                </IonItem>
              </IonMenuToggle>
              <IonMenuToggle onClick={() => onClick("/withdraw")}>
                <IonItem>
                  <IonLabel>
                    <MdNotifications /> Withdraw
                  </IonLabel>
                </IonItem>
              </IonMenuToggle>
              <IonMenuToggle onClick={() => onClick("/invite")}>
                <IonItem>
                  <IonLabel>
                    <MdAccountBalanceWallet /> Invite a friend
                  </IonLabel>
                </IonItem>
              </IonMenuToggle>
              <IonMenuToggle onClick={() => onClick("")}>
                <IonItem>
                  <IonLabel>
                    <MdOutlineContactSupport /> Support
                  </IonLabel>
                </IonItem>
              </IonMenuToggle>
            </IonList>
          </div>
        </IonContent>
        <IonFooter className={"profileList"}>
          <IonMenuToggle>
            <IonItem onClick={() => logout()}>
              <IonLabel>
                <CgLogOut /> Sign Out
              </IonLabel>
            </IonItem>
          </IonMenuToggle>
        </IonFooter>
      </IonMenu>
      <IonRouterOutlet id="main"></IonRouterOutlet>
    </>
  );
};
