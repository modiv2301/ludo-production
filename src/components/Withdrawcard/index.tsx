import classes from './index.module.scss';
import { IonAvatar } from '@ionic/react';
import { Box, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { CgProfile } from 'react-icons/cg';
import money from './../../assets/img/money.svg';
import male from './../../assets/img/male.png';
import female from './../../assets/img/female.png';
import battle from './../../assets/img/energy.png';
import { useHistory } from 'react-router';


import {
    IonCardContent,
    IonCard,
    IonButton,
    IonIcon,
} from "@ionic/react";

import { FaRupeeSign } from "react-icons/fa";
// import { allgame, getprofilewithdrawamount, postwithdrawamount, getentryamount } from '../../apis';

import { CircularProgress, Stack, TextField } from '@mui/material';

import { postwithdrawamount } from '../../apis';


export const Withdrawcard = () => {


    const [amount, setamount] = React.useState<string>()
    const history = useHistory();


    const submit = async () => {

        const data = {
            withdraw_amount: amount,

        }

        // console.log("data =>"+JSON.stringify(data))

        let walletAdd = await postwithdrawamount(data)
        .then((response)=> {
            history.push('/')
        });

        console.log("my result is =>" + JSON.stringify(walletAdd))


    }


    return (
        <div style={{
            alignItems: 'center',
            padding:"2rem",
    display: "flex",
    flexDirection: "column",
    
    height: '80%',
    justifyContent: 'center'
        }}>
            <TextField type="text" placeholder='enter amount to withdraw' label="Amount"
                id="outlined-size-small"
                value={amount}
                onChange={event => setamount(event.target.value)}
                size="medium"
                fullWidth
                color="primary"
            />
            <br />
            <IonButton onClick={submit}>submit</IonButton>
        </div>
    )
}

