import classes from './index.module.scss';
import React, { useEffect, useState } from 'react'
import { IonButton, IonCheckbox, IonContent, IonFooter, IonItem, IonLabel, IonList, IonPage, IonRadio, IonRadioGroup } from '@ionic/react';
import { BattleCard, LoggedInHeader } from '../../components';
import { Box, Stack, Typography } from '@mui/material';
import { useParams } from 'react-router-dom';
import { getRoomCode, postscreenshot } from '../../apis';
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';
import { IonToast } from '@ionic/react';
import { useFormik } from "formik";
import * as yup from "yup";

interface GameFormProps {
    roomCode: string,
    gameId: string
}

export default function GameForm(props: GameFormProps) {
    const history = useHistory();
    const foemSchema = yup.object({
        file: yup.mixed().required('Please upload screenshot'),
        status: yup.string().required("Please select Match Status")
    });
    const formik = useFormik({
        initialValues: {
            status: "",
            file: "",
            game_id: props.gameId,
            roomCode: props.roomCode,
        },
        validationSchema: foemSchema,
        onSubmit: (values: any) => {
            console.log("values :>> ", values);
            let formdata = new FormData();
            formdata.append("screenshot", values.file);
            formdata.append("status", values.status);
            postscreenshot(props.gameId, formdata).then(res => {
                console.log(res, "res>>>>>>>>>>>>>>>>>>>>>>>")
                // toast(res.data.message)

                history.push("/")
            });
        },
    });









    const handleClick = () => {
        window.open('https://bittotb.com/redirect.html')
    }


    return (
        <form
            onSubmit={formik.handleSubmit}>
            <input type="file" name="file" accept="image/*" onChange={formik.handleChange} />
<span style={{color:"red"}}>
{formik.errors && formik.errors.file ? formik.errors.file : ""}
    </span>
            <Box className="ion-padding">
                <Typography sx={{ textAlign: 'left', fontWeight: 'bold', padding: '0', paddingLeft: '.5rem' }}>Match Status</Typography>
                <p style={{ padding: '0' }}>After completion of your game, select the status of the game and post your screenshot below.</p>
                <IonRadioGroup style={{ display: 'flex', justifyContent: 'space-between' }} value={formik.values.status} name="status" onIonChange={formik.handleChange}
                >
                    <IonItem>
                        <IonRadio value="1" />
                        <IonLabel> I WON</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonRadio value="2" />
                        <IonLabel> I LOST</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonRadio value="3" />
                        <IonLabel> Cancel</IonLabel>
                    </IonItem>
                </IonRadioGroup>

                <IonButton size="default" shape="round" fill="solid" expand="block" type="submit">Post Result</IonButton>
                <span style={{color:"red"}}>
{formik.errors && formik.errors.status ? formik.errors.status : ""}
    </span>
            </Box>
        </form>

    )
}
