import classes from './index.module.scss';
import React, { useEffect, useState } from 'react'
import { IonButton, IonCheckbox, IonContent, IonFooter, IonItem, IonLabel, IonList, IonPage, IonRadio, IonRadioGroup } from '@ionic/react';
import { BattleCard, LoggedInHeader } from '../../components';
import { Box, Stack, Typography } from '@mui/material';
import { useParams } from 'react-router-dom';
import { getRoomCode, postscreenshot } from '../../apis';
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';
import { IonToast } from '@ionic/react';
import GameForm from './GameForm';
interface CreateBattleProps { };
interface ParamTypes {
    id: string,
    roomCode: string
}
export const CreateBattle = (props: CreateBattleProps) => {

    const params = useParams<ParamTypes>();
    console.log("params:>>", params.id)
    const history = useHistory();

    let [roomCode, setRoomCode] = useState({
        "roomcode": "",
        "battle_id": "",
        "created_at": "",
        "deleted_at": "",
        "id": "",
        "is_valid": "",
        "left_use": "",
        "max_use": "",
        "updated_at": ""
    });

    const [file, setfile] = useState()
    const [errorMessage, setErrorMessage] = useState(false)

    const showRoomCodeApi = (id) => {
        getRoomCode(id)
            .then((response) => {
                console.log("response", response)
                if (!response.data.status) {
                    setErrorMessage(response.data.message)
                }
                setRoomCode(response.data.data);
            })
    }
    useEffect(() => {
        console.log(params.id, "params:>>>>>>")
        if (params.roomCode) {
            setRoomCode(
                {
                    ...roomCode,
                    roomcode: params.roomCode,
                    battle_id: params.id
                })
        } else {
            showRoomCodeApi(params.id)

        }
    }, [])

    const handlechange = (event) => {
        setfile(event.target.files[0])
    }

    const fileupload = async () => {

        let data1 = {
            "user_id": localStorage.getItem("userid"),
        }

        let data = {
            status: 1,
            screenshot: file
        }


        let walletAdd = await postscreenshot(data1, data);

        console.log("my image upload", JSON.stringify(walletAdd))
    }


    const handleClick = () => {
        window.open('https://bittotb.com/redirect.html')
    }


    return (
        <IonPage>
            <IonContent fullscreen>
                <LoggedInHeader />
                <Box sx={{
                    textAlign: 'center',
                    height: '12rem',
                    background: '#ebebeb',
                    margin: '1rem',
                    borderRadius: '15px',
                }}>
                    <div className={classes.code} style={{ paddingTop: '2rem', fontWeight: 'bold', fontSize: 'x-large', color: 'gray' }}>
                        {errorMessage ? errorMessage : <>
                            <div>Room Code</div>
                            <div style={{ color: '#60b783' }}>{roomCode.roomcode}</div>
                            <IonButton size="default" shape="round" onClick={handleClick}>Copy Code</IonButton>
                        </>}

                    </div>
                </Box>
                <Box>
                    <Typography sx={{ textAlign: 'center' }}>Play Ludo game in Ludo King App</Typography>
                    <Box sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: '2rem'
                    }}>
                        <Box className={classes.playStore}></Box>
                        <Box className={classes.appStore}></Box>
                    </Box>
                </Box>
                <Box className="ion-padding" sx={{ backgroundColor: 'lightyellow' }}>
                    <Typography sx={{ textAlign: 'center', fontWeight: 'bold' }}>Game Rules</Typography>
                    <IonList className="ion-padding">
                        <IonItem style={{ fontSize: '.8rem' }}>Record Every Game While Playing</IonItem>
                        <IonItem style={{ fontSize: '.8rem' }}>For Cancellation of game, video proof is necessary</IonItem>
                        <IonItem style={{ fontSize: '.8rem' }}>50 Penality will be charged for updating wrong result</IonItem>
                        <IonItem style={{ fontSize: '.8rem' }}>25 penality will be charged for not updating result</IonItem>
                    </IonList>
                </Box>

                {params.roomCode ? <GameForm roomCode={roomCode.roomcode} gameId={params.id}/> : ""}

            </IonContent>
            {/* <IonFooter>
                <Typography sx={{ textAlign: 'center' }}>Play Ludo game in Ludo King App</Typography>
                <Box sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    padding: '2rem'
                }}>
                    <Box className={classes.playStore}></Box>
                    <Box className={classes.appStore}></Box>
                </Box>
            </IonFooter> */}
        </IonPage>
    )
}
