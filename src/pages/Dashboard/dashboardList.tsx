import classes from './index.module.scss';
import { IonAvatar } from '@ionic/react';
import { Box, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { CgProfile } from 'react-icons/cg';
import money from './../../assets/img/money.svg';
import male from './../../assets/img/male.png';
import female from './../../assets/img/female.png';
import battle from './../../assets/img/energy.png';
import { useHistory } from 'react-router';

import {
    IonContent,
    IonPage,
    IonCardContent,
    IonCard,
    IonButton,
    IonIcon,
    IonBadge,
    IonFooter,
} from "@ionic/react";

import { FaRupeeSign } from "react-icons/fa";
import { allgame } from '../../apis';
import { useIonViewDidEnter } from '@ionic/react';


interface BattleCardProps {
    username1?: string;
    username2?: string;
    amount?: number;
    onClick?: Function;
};

export const DashboardList = () => {

    let [battleList, setBattleList] = useState<any[]>([]);
   
    const history = useHistory();


    const getallgame = async () => {
        try {

            let response = await allgame();
            // console.log("my battle record" + JSON.stringify(response.data.data));
            setBattleList(response.data.data)

        } catch (error) {
            console.error("Getting error at auth: ", error);

        }
    };
    
    useIonViewDidEnter(() => {     
        getallgame()
    }, [])

    return (
        <>
            {
                battleList.map((title,index) =>(
                    <IonCard key={index}>
                    <IonCardContent
                        style={{
                            textTransform: "uppercase",
                        }}
                    >
                        {title.game_name}
                        <span
                            style={{
                                textTransform: "uppercase",
                                float: "right",
                            }}
                        >
                            entry
                        </span>
                    </IonCardContent>
    
                    <IonCardContent
                        style={{
                            color: "5px solid black",
                        }}
                    >
                        <IonIcon
                            style={{ width: "2rem", height: "2rem", verticalAlign: "middle" }}
                            src={money}
                        />
                        &nbsp; {title.win_amount}
                        <span
                            style={{
                                textTransform: "uppercase",
                                float: "right",
                                // marginTop: "-3%",
                            }}
    
    
                        >
                            <IonButton color="primary" id="btn"
                              onClick={()=> history.push('/create-battle/'+title.id)}
                            >
                                <FaRupeeSign />
                                {title.entry_amount}
                            </IonButton>
                        </span>
                    </IonCardContent>
    
                    {/* <IonFooter
    
                        id="footer"
    
                    >
                        <div
                            style={{
                                color: "red",
                                textTransform: "uppercase",
                            }}
    
                            id="play"
    
                        >
                            Playing Now
                        </div>
                    </IonFooter> */}
                </IonCard>
                ))
            }
</>
           
    )
}
