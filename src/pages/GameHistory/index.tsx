import './index.scss';
import React, { useEffect, useState } from 'react';
import { IonContent, IonPage } from '@ionic/react';
import { LoggedInHeader } from '../../components';
import { useHistory } from 'react-router';
import { Box, Typography } from '@mui/material';
import nogames from './../../assets/img/nogamehistory.png';
import './style.css';
import DataTable from 'react-data-table-component';
import { getGameHistory } from '../../apis';

const columns = [
    {
        name: 'Room Code',
        selector: row => row.title,
    },
    {
        name: 'Win/Loose',
        selector: row => row.winLoose ,
    },
];

export const GameHistory = () => {
    const [gameHistoryList, setGameHistoryList] = useState([]);
    const history = useHistory();
    useEffect(()=> {
        getGameHistory()
        .then((response)=> {
            // console.log('response :>> ', response);
           let data =  response.data.data.data.map((row, index)=> {
            console.log('row', row);
                let winLoose = row.status == 0 ? <button className='btn btn-primary' onClick={() => {history.push(`/game-history/${row.id}/${row.roomcode}`)}}>Pending</button> : row.status == 1 ? "Win" : "Loose";
                return {title:row.roomcode, winLoose}
            })
            // console.log('data', data)
            setGameHistoryList(data); 
            
        })
        .catch((error)=>{
            console.error("Getting error at auth: ", error);
        })
      
    },[])
    
    console.log("gameHistoryList:>>",gameHistoryList)
    return (
        <IonPage>
            <IonContent fullscreen>
                <LoggedInHeader />
                <DataTable
                    columns={columns}
                    data={gameHistoryList}
                />
            </IonContent>
        </IonPage>
    )
}