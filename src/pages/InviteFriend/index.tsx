import styles from "./index.module.scss";
import React, { useEffect, useState } from "react";
// import { IonButton, IonContent, IonPage, IonToolbar } from "@ionic/react";
import {
  IonContent,
  IonButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonHeader,
  IonLabel,
  IonIcon,
  IonItem,
  IonCardContent,
  IonCardTitle,
  IonCardSubtitle,
  IonCardHeader,
  IonCard,
} from "@ionic/react";
import { useHistory } from "react-router";
import { auth } from "../../apis";
import { setStore, storeKeys } from "../../apis/constants";
import { Box, Typography, Stack } from "@mui/material";

import { LoggedInHeader } from "../../components";

import { Withdrawcard } from "../../components";

export const InviteFriend = () => {
  let [refferalCode, setRefferalCode] = useState(localStorage.getItem("referral_code"));
  return (
    <IonPage>
      <IonContent fullscreen>
        <LoggedInHeader />
        
        <h1 style={{textAlign: 'center', paddingTop:'50px'}}>Your refference code</h1>
        <Stack spacing={3} direction="column" justifyContent="center">
          <IonButton
            className="ion-margin-top"
            expand="full"
            shape="round"
            size="large"
            fill={"solid"}
            onClick={() => {navigator.clipboard.writeText(refferalCode)}}
          >
            {localStorage.getItem("referral_code")}
          </IonButton>
        </Stack>
      </IonContent>
    </IonPage>
  );
};
