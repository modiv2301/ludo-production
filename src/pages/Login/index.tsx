import styles from "./index.module.scss";
import React, { useEffect, useState } from "react";
import { IonButton, IonContent, IonPage, IonToolbar } from "@ionic/react";
import { CircularProgress, Stack, TextField, Typography } from "@mui/material";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { Header } from "../../components";
import { useHistory } from "react-router";
import { arrowUndoSharp } from "ionicons/icons";
import { auth, sendOtpForLogin } from "../../apis";
import { setStore, storeKeys } from "../../apis/constants";
import { getprofilewithdrawamount } from "../../apis";
import TextInput from "../../components/FormFields/TextInput";

import { useFormik } from "formik";
import * as yup from "yup";
interface LoginProps { }

export const Login = (props: LoginProps) => {
  const history = useHistory();

  const storetoken = localStorage.getItem("usertoken");

  useEffect(() => {
    if (storetoken == null) {
      history.push("/login");
    } else {
      history.push("/dashboard");
    }
  }, []);

  const [loading, setLoading] = React.useState<boolean>(false);
  const [showOtpField, setShowOtpField] = React.useState<boolean>(false);
  const [showError, setShowError] = React.useState<boolean>(false);

  const onLogin = async (data) => {
    try {
      setLoading(true);
      const response: Record<string, any> = (await auth(data)).data;

      // console.log(response);
      if (response.status) {
        setShowOtpField(false);

        localStorage.setItem(
          "usertoken",
          JSON.stringify(response.token).replaceAll('"', "")
        );
        localStorage.setItem(
          "userimage",
          JSON.stringify(response.data.image).replaceAll('"', "")
        );
        localStorage.setItem(
          "username",
          JSON.stringify(response.data.name).replaceAll('"', "")
        );
        localStorage.setItem(
          "userid",
          JSON.stringify(response.data.id).replaceAll('"', "")
        );
        localStorage.setItem(
          "useremail",
          JSON.stringify(response.data.email).replaceAll('"', "")
        );
        localStorage.setItem(
          "useramount",
          JSON.stringify(response.data.wallet_amount).replaceAll('"', "")
        );
        localStorage.setItem(
          "referral_code",
          JSON.stringify(response.data.referral_code).replaceAll('"', "")
        );
        setStore(storeKeys.response, response.data);
        setStore(storeKeys.access_token, response.token);
        history.push("/dashboard");
      } else  {
        console.log("response.status", response.status);
        setShowError(response.message)

      } 
      setLoading(false);
    } catch (error) {
      console.error("Getting error at auth: ", error);
      setLoading(false);
    }
  };

  const loginSchema = yup.object({
    mobile_no: yup
      .number()
      .typeError("you must specify a number")
      .required()
      .test(
        "Mobile length",
        "Mobile Number should be 10 digits",
        (value) => value && value.toString().length == 10
      )
      .test(
        "Mobile length",
        "Mobile Number should be 10 digits", // <- key, message
        async (value) => {
          return value && value.toString().length == 10 ? true : false;
        }
      ),
    // please call api on true condition  pattern is
    // validate({ email: value }).then((res) => {
    //     console.log("res", res);
    //     if (res.status) {
    //       return true;
    //     } else {
    //       return false;
    //     }
    //   })
    otp: yup.string().required("Please Enter Otp"),
  });
  const formik = useFormik({
    initialValues: {
      mobile_no: "",
      otp: "",
      device_id: "asgsdf",
      device_type: "android",
    },
    validationSchema: loginSchema,
    onSubmit: (values: any) => {
      console.log("values :>> ", values);
      onLogin(values);
    },
  });

  const sendOtp = async () => {
    try {
      setLoading(true);
      const response: Record<string, any> = (await sendOtpForLogin({ mobile_no: formik.values.mobile_no })).data;
      console.log("response", response);
      if (response.errors) {
        console.log("response.errors", response.errors);
      } else if (!response.status) {
        console.log("response.status", response.status);
        setShowError(response.message);

      } else {
        setShowOtpField(true);
        setShowError(false)
      }
      setLoading(false);
    } catch (error) {
      console.error("Getting error at auth: ", error);
      setLoading(false);
    }
  };
  return (
    <IonPage>
      <IonContent className={styles["login-form"]} fullscreen>
        <Header icon={arrowUndoSharp} onIconClick={() => history.goBack()} />
        <form
          // onSubmit={onLogin}
          onSubmit={formik.handleSubmit}
          className={`ion-padding `}
          style={{ paddingTop: "20%" }}
        >
          <div className="ion-padding">
            <Stack spacing={3} direction="column" justifyContent="center">
              <IonToolbar>
                <Typography
                  variant="h5"
                  style={{ fontWeight: "bolder", fontSize: "xx-large" }}
                >
                  <FormattedMessage id="login.title" />
                </Typography>
                <Typography
                  variant="subtitle1"
                  sx={{ fontWeight: "bolder", color: "gray" }}
                >
                  <FormattedMessage id="login.sub_title" />
                </Typography>
              </IonToolbar>

              {showOtpField ? (
                <>
                  <div>
                    <TextInput
                      formik={formik}
                      label={"OTP"}
                      field_name="otp"
                      className={styles.passwordInput}
                    />
                  </div>
                  <span style={{ color: "red" }}>{showError}</span>

                  <div>
                    <IonButton
                      type="submit"
                      className="ion-margin-top"
                      expand="full"
                      shape="round"
                      size="large"
                      fill={loading ? "outline" : "solid"}
                      disabled={
                        Object.keys(formik.errors).length ? true : false
                      }
                    >
                      {loading ? (
                        <CircularProgress color="success" />
                      ) : (
                        <FormattedMessage id="login.button_text" />
                      )}
                    </IonButton>
                  </div>
                </>
              ) : (
                <>
                  <div>
                    <TextInput
                      formik={formik}
                      label={"Mobile Number"}
                      field_name="mobile_no"
                      className={styles.emailInput}
                    />
                  </div>
                  <span style={{ color: "red" }}>{showError}</span>
                  <Typography
                    sx={{
                      textAlign: "right",
                      fontWeight: "bold",
                      color: "gray",
                      padding: "0",
                      paddingTop: ".5rem",
                    }}
                  >
                    <IonButton
                      className="ion-margin-top"
                      shape="round"
                      size="large"
                      expand="full"
                      disabled={
                        formik.errors.mobile_no || !formik.values.mobile_no
                          ? true
                          : false
                      }
                      onClick={sendOtp}
                      fill={loading ? "outline" : "solid"}
                    >
                      Send Otp
                    </IonButton>
                  </Typography>
                </>
              )}
              <Typography
                sx={{
                  textAlign: "left",
                  fontWeight: "bold",
                  color: "gray",
                  padding: "0",
                  marginTop: "5px !important",
                }}
              >
                <FormattedMessage id="login.dont_have_account" />{" "}
                <Link to="/signup" style={{ textDecoration: "none" }}>
                  <FormattedMessage id="login.signup" />
                </Link>
              </Typography>
            </Stack>
          </div>
        </form>
      </IonContent>
    </IonPage>
  );
};
