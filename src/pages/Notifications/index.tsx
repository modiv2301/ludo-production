import './index.scss';
import React, { useEffect, useState } from 'react';
import {
    IonContent, IonPage, IonTitle, IonToolbar,
    IonHeader, IonLabel, IonIcon, IonItem, IonCardContent, IonCardTitle, IonCardSubtitle, IonCardHeader, IonCard
} from '@ionic/react';
import { LoggedInHeader } from '../../components';
import { useHistory } from 'react-router';
import { Box, Typography } from '@mui/material';
import nonotification from './../../assets/img/nonotification.png';
import { getNotification } from '../../apis';

export const Notification = () => {

    const [data, setdata] = useState([]);

    const [isloading, setisloading] = useState(false);


    useEffect(()=>{
        try {
            let response = getNotification()
            .then((response)=>{
                console.log("my battle record" + JSON.stringify(response.data.data));
                setdata(response.data.data.data);
            })
            .catch ((error) => {
                console.error("Getting error at auth: ", error);
    
            })

        } catch (error) {
            console.error("Getting error at auth: ", error);

        }
    },[])

    console.log(data,"data:>>>")
    


    const getnoti = () => {
        setisloading(true)
    }


    useEffect(() => {
        getnoti()
    }, [])

    const history = useHistory();


    return (
        <IonPage>
            <IonContent fullscreen>
                <LoggedInHeader />
                <Box className="ion-padding centerimg">
                    {/* <img src={nonotification} /> */}

                    <IonCard>
                        {/* <IonCardHeader>
                            <IonCardTitle>Card Title</IonCardTitle>
                        </IonCardHeader> */}
                        {
                            data.length ? data.map((item) => {
                                console.log("item:>>", item)
                                return(
                                    <>

                                        <IonCardContent className='mt-2 pt-2' style={{
                                            width: '90rem',
                                            color: 'black'
                                        }}>
                                            {item.json_data.message}
                                        </IonCardContent>
                                    </>
                                )

                            }) : 
                            <IonCardContent className='mt-2 pt-2' style={{
                                width: '90rem',
                                color: 'black'
                            }}>
                               No Notifications
                            </IonCardContent>
                        }

                    </IonCard>
                </Box>
            </IonContent>
        </IonPage>
    )
}