import styles from "./index.module.scss";
import React from "react";
import { IonButton, IonContent, IonPage, IonToolbar } from "@ionic/react";
import { Header } from "../../components";
import { CircularProgress, Stack, Typography } from "@mui/material";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { useHistory, useParams } from "react-router";
import { arrowUndoSharp } from "ionicons/icons";
import { registerSendOtp, register } from "../../apis";
import { setStore, storeKeys } from "../../apis/constants";
import TextInput from "../../components/FormFields/TextInput";

import { useFormik } from "formik";
import * as yup from "yup";

interface SignupProps {}

export const Signup = (props: SignupProps) => {
  const history = useHistory();

  const [loading, setLoading] = React.useState<boolean>(false);
  const [showOtpField, setShowOtpField] = React.useState<boolean>(false);

  const onRegister = async (data: any) => {
    try {
      setLoading(true);
      const response: Record<string, any> = (await register(data)).data;
      // console.log({ email, password, response });
      if (response.errors) {
        if (response.errors?.name) {
        }
        if (response.errors?.email) {
        }
      } else if (!response.status) {
      } else {
        setStore(storeKeys.response, response.data);
        setStore(storeKeys.access_token, response.token);
        history.push("/login");
      }
      setLoading(false);
    } catch (error) {
      console.error("Getting error at auth: ", error);
      setLoading(true);
    }
  };

  const signupSchema = yup.object({
    mobile_no: yup
      .number()
      .typeError("you must specify a number")
      .required("Please Enter Mobile")
      .test(
        "Mobile length",
        "Mobile Number should be 10 digits",
        (value) => value && value.toString().length == 10
      ),
    name: yup.string().required("Please Enter Name"),
    referral_code: yup.string(),
    // password: yup.string().required("Please Enter Password"),
  });
  const formik = useFormik({
    initialValues: {
      mobile_no: "",
      name: "",
      //   password: "",
      device_id: "",
      referral_code: "",
    },
    validationSchema: signupSchema,
    onSubmit: (values: any) => {
      console.log("values :>> ", values);
      onRegister(values);
    },
  });

  console.log("formik.isSubmitting :>> ", formik.isSubmitting, formik.errors);
  const sendOtp = () => {
    let data: any = {
      ...formik.values,
    };
    registerSendOtp(data)
      .then((res) => {
        console.log("res :>> ", res);
        setShowOtpField(true);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  return (
    <IonPage>
      <IonContent className={styles["login-form"]} fullscreen>
        <Header icon={arrowUndoSharp} onIconClick={() => history.goBack()} />
        <form
          //   onSubmit={onRegister}
          onSubmit={formik.handleSubmit}
          className={`ion-padding `}
          style={{ paddingTop: "10%" }}
        >
          <div className="ion-padding">
            <Stack spacing={3} direction="column" justifyContent="center">
              <IonToolbar>
                <Typography
                  variant="h5"
                  style={{ fontWeight: "bolder", fontSize: "xx-large" }}
                >
                  <FormattedMessage id="signup.title" />
                </Typography>
                <Typography
                  variant="subtitle1"
                  sx={{ fontWeight: "bolder", color: "gray" }}
                >
                  <FormattedMessage id="signup.sub_title" />
                </Typography>
              </IonToolbar>
              <div>
                <TextInput
                  formik={formik}
                  label={"Username"}
                  field_name="name"
                />
              </div>
              <div>
                <TextInput
                  formik={formik}
                  label={"Mobile"}
                  field_name="mobile_no"
                />
              </div>
              <div>
                <div>
                  <TextInput
                    formik={formik}
                    label={" Reffaral Code (Optional)"}
                    field_name="reffaral_code"
                  />
                </div>

                <Typography
                  sx={{
                    textAlign: "left",
                    fontWeight: "bold",
                    color: "gray",
                    padding: "0",
                    paddingTop: ".5rem",
                  }}
                >
                  <FormattedMessage id="signup.declaration" />
                  <Link to="/terms-policies" style={{ textDecoration: "none" }}>
                    <FormattedMessage id="signup.terms_conditions" />
                  </Link>
                  <FormattedMessage id="signup.declaration_conjuction" />
                  <Link to="/terms-policies" style={{ textDecoration: "none" }}>
                    <FormattedMessage id="signup.privacy_policy" />
                  </Link>
                </Typography>
              </div>
              <div>
                {!showOtpField ? (
                  <IonButton
                    type="button"
                    className="ion-margin-top"
                    expand="full"
                    fill={loading ? "outline" : "solid"}
                    shape="round"
                    size="large"
                    disabled={
                      formik.errors.mobile_no || !formik.values.mobile_no
                        ? true
                        : false
                    }
                    onClick={sendOtp}
                  >
                    {loading ? (
                      <CircularProgress color="success" />
                    ) : (
                      "Send Otp"
                    )}
                  </IonButton>
                ) : (
                  <>
                    <div>
                      <TextInput
                        formik={formik}
                        label={"OTP"}
                        field_name="otp"
                        className={styles.passwordInput}
                      />
                    </div>
                    <div>
                      <IonButton
                        type="submit"
                        className="ion-margin-top"
                        expand="full"
                        shape="round"
                        size="large"
                        fill={loading ? "outline" : "solid"}
                        disabled={
                          Object.keys(formik.errors).length ? true : false
                        }
                      >
                        {loading ? (
                          <CircularProgress color="success" />
                        ) : (
                          <FormattedMessage id="signup.button_text" />
                        )}
                      </IonButton>
                    </div>
                  </>
                )}
                <Typography
                  sx={{
                    textAlign: "left",
                    fontWeight: "bold",
                    color: "gray",
                    padding: "0",
                    paddingTop: ".5rem",
                  }}
                >
                  <FormattedMessage id="signup.already_have_account" />{" "}
                  <Link to="/login" style={{ textDecoration: "none" }}>
                    <FormattedMessage id="signup.signin" />
                  </Link>
                </Typography>
              </div>
            </Stack>
          </div>
        </form>
      </IonContent>
    </IonPage>
  );
};
