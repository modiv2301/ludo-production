import "./index.scss";
import React from "react";
import {
  IonContent,
  IonPage,
  IonCardContent,
  IonCard,
  IonButton,
  IonIcon,
  IonBadge,
  IonFooter,
} from "@ionic/react";
import { LoggedInHeader } from "../../components";
import { useHistory } from "react-router";
import { Box, Typography, Button } from "@mui/material";
import { FaRupeeSign } from "react-icons/fa";
// import notransactionhistory from './../../assets/img/notransactionhistory.png';
import money from "./../../assets/img/money.svg";

export const TransactionHistory = () => {

  const history = useHistory();

  var data = [
    {
      id: 1,
      name: "aysuh dubey",
      title: "your credit balance",
      amount: "+10000",
      status: 0,
    },
    {
      id: 2,
      name: "niraj mishra",
      title: "your credit balance",
      amount: "+10000",
      status: 0,
    },
    {
      id: 4,
      name: "rishabh mishra",
      title: "your credit balance",
      amount: "+10000",
      status: 0,
    },
    {
      id: 5,
      name: "shinu tech",
      title: "your debit balance",
      amount: -100,
      status: 1,
    },
    {
      id: 6,
      name: "umesh yadav",
      title: "your debit balance",
      amount: "-50000",
      status: 1,
    },
    {
      id: 7,
      name: "deepak singh",
      title: "your debit balance",
      amount: -500,
      status: 1,
    },
  ];

  return (
    <IonPage>
      <IonContent>
        <LoggedInHeader />

        <div
          style={{
            marginTop: "5%",
          }}
        ></div>

        <IonCard>
          <IonCardContent
            style={{
              textTransform: "uppercase",
            }}
          >
            prize pool
            <span
              style={{
                textTransform: "uppercase",
                float: "right",
              }}
            >
              entry
            </span>
          </IonCardContent>

          <IonCardContent
            style={{
              color: "5px solid black",
            }}
          >
            <IonIcon
              style={{ width: "2rem", height: "2rem", verticalAlign: "middle" }}
              src={money}
            />
            &nbsp; 20
            <span
              style={{
                textTransform: "uppercase",
                float: "right",
                // marginTop: "-3%",
              }}

           
            >
              <IonButton color="primary"    id="btn"
>
                <FaRupeeSign />
                12
              </IonButton>
            </span>
          </IonCardContent>

          <IonFooter
        
            id="footer"

          >
            <div
              style={{
                color: "red",
                textTransform: "uppercase",
              }}

              id="play"

            >
              Playing Now
            </div>
          </IonFooter>
        </IonCard>

        <IonCard>
          <IonCardContent
            style={{
              textTransform: "uppercase",
            }}
          >
            prize pool
            <span
              style={{
                textTransform: "uppercase",
                float: "right",
              }}
            >
              entry
            </span>
          </IonCardContent>

          <IonCardContent
            style={{
              color: "5px solid black",
            }}
          >
            <IonIcon
              style={{ width: "2rem", height: "2rem", verticalAlign: "middle" }}
              src={money}
            />
            &nbsp; 20
            <span
              style={{
                textTransform: "uppercase",
                float: "right",
              }}
            >
              <IonButton color="primary" id="btn">
                <FaRupeeSign />12
              </IonButton>
            </span>
          </IonCardContent>

          <IonFooter
           
            id="footer"
          >
            <div
              style={{
                color: "red",
                textTransform: "uppercase",
              }}
              id="play"
            >
              Playing Now
            </div>

            <div
              id="waiting"
            >
              Waiting now
            </div>
          </IonFooter>
        </IonCard>
        
      </IonContent>
    </IonPage>
  );
};
