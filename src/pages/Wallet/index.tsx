import classes from './index.module.scss';
import React, { useEffect, useState } from 'react';
import { IonContent, IonPage } from '@ionic/react';
import { LoggedInHeader, WalletCard } from '../../components';
import { Stack } from '@mui/material';
import { wallet } from '../../apis';


interface WalletProps { };

export const Wallet = (props: WalletProps) => {

    const [walletHistory, setWalletHistory] = useState<any[]>([]);

    // let storetoken = localStorage.getItem("usertoken")


    const getallgame = async () => {
        try {

            let data = {
                "user_id": localStorage.getItem("userid"),  
            }

            let response = await wallet(data);
            // console.log("my wallet history record" + JSON.stringify(response.data));
            setWalletHistory(response.data.data.data)

        } catch (error) {
            console.error("Getting error at auth: ", error);

        }
    };

    useEffect(() => {
    //     console.log("is this comingggggggggggggg")

      

    //    (data)
    //     .then((response)=> {
    //         console.log(response.data.data)
    //         setWalletHistory(response.data.data)
    //     })
    //     .catch((error)=>{
    //         console.error("Getting error at auth: ", error);
    //     })
    
    getallgame()

    }, [])
    console.log("walletHistory:>>", walletHistory)
    return (
        <IonPage>
            <IonContent fullscreen>
                <LoggedInHeader />

                {walletHistory.length ? walletHistory.map((transaction, index) => (
                    <WalletCard
                        key={index}
                        buttonTitle={transaction.status == 1 ? (transaction.title == "Add Money To Wallet." ? "Deposit Cash" : "Win in match") : "Played game"}
                        subheading={transaction.status == 1 ? (transaction.title == "Add Money To Wallet." ? "Deposit Cash" : "Win in match") : "Played game"}
                        amount={transaction.wallet_amount} description=""
                    />
                )) : ""}


            </IonContent>
        </IonPage>
    )
}

