import styles from './index.module.scss';
import React , { useEffect, useState }from 'react';
// import { IonButton, IonContent, IonPage, IonToolbar } from '@ionic/react';
import {
    IonContent, IonPage, IonTitle, IonToolbar,
    IonHeader, IonLabel, IonIcon, IonItem, IonCardContent, IonCardTitle, IonCardSubtitle, IonCardHeader, IonCard
} from '@ionic/react';
import { useHistory } from 'react-router';
import { auth } from '../../apis';
import { setStore, storeKeys } from '../../apis/constants';
import { Box, Typography } from '@mui/material';

import { LoggedInHeader } from '../../components';

import { Withdrawcard} from '../../components';




export const Withdraw = () => {

    

  
    return (
        <IonPage>
        <IonContent fullscreen>
            <LoggedInHeader />
           
                <Withdrawcard/>
        
        </IonContent>
    </IonPage >
    )
};

  